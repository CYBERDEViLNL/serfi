#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/stat.h>
#include <signal.h>

#define TERM_BYTE	0x00
#define EXEC_BYTE	0x01
#define OP_SERVO	0x02

/* Sources:
 *	http://man7.org/linux/man-pages/man3/termios.3.html
 *	https://en.wikibooks.org/wiki/Serial_Programming/termios#open.282.29
 *	https://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html
 * 	https://www.gnu.org/software/libc/manual/html_node/Signal-Handling.html
 *	http://man7.org/linux/man-pages/man3/strtol.3.html
 * */
 
 /*	current TODO
  * error checks on fd opening and closing.
  * */
 
// gimme bools
typedef enum { false, true } bool;
 
struct servoData {
  char axis;
  unsigned char angle;
};

char opcode = (char)OP_SERVO;
char exec = (char)EXEC_BYTE;
char term = (char)TERM_BYTE;

void write_servo(int fd, struct servoData* p_servo_data){
	write(fd,&opcode,1);				// send opcode
	write(fd,&p_servo_data->axis,1);	// send axis data
	write(fd,&p_servo_data->angle,1);	// send angle data
	write(fd,&exec,1);					// send execute byte
}
bool file_exists(const char * file){
	if(access( file, F_OK ) == -1){
		printf("%s does not exist.\n", file);
		return false;
	} else {
		return true;
	}
}
bool file_write_access(const char * file){
	if(access( file, W_OK ) == -1){
		printf("you don't have write permissions for %s.\n", file);
		return false;
	} else {
		return true;
	}
}
int open_fd(const char * device, int oflags){
	int fd = open(device, oflags);
	if(fd == -1) {
		printf("failed to open port\n");
		return -1;
	} else {
		printf("connection opened: %d %s\n",fd,device);
		return fd;
	}
}

int main(int argc,char** argv){
		int c;
		bool daemon = false;
		bool kill_daemon = false;

		struct servoData servo_data;

		// dump for stroul
		char *strtoul_tmp = NULL;
		
		// parse arguments
		while ((c = getopt (argc, argv, "dqx:y:")) != -1) {
			switch(c){
				case 'x': case 'y':
					if(optarg){
						servo_data.axis = (char)c;
						if(strtoul(optarg, &strtoul_tmp, 0) >= 0 && strtoul(optarg, &strtoul_tmp, 0) <= 180){
							servo_data.angle = strtoul(optarg, &strtoul_tmp, 0);
						}
					}
					break;
				case 'd'://daemon
					daemon = true;
					break;
				case 'q':
					kill_daemon = true;
					break;
			}
		}

		// create pointer
		struct servoData* p_servo_data;
		p_servo_data = &servo_data;

		// if daemon
		int	fd_fifo;
		const char *fifo = "/tmp/SERVO";
		char fifo_buff[4];// opcode,axis,angle,exec thus 4
		if (daemon == true){
			const char *device;
			// never trust user input TODO
			if(optind && argv[optind]){
				device = argv[optind];
			} else{
				printf("path to tty not valid\n");
				printf("example: `./servo -d /dev/ttyUSB0`\n");
				return 0;
			}

			mkfifo(fifo, 0666);
			struct termios config;

			int fd; // file descript0r
			if(file_exists(device) == false && 
			file_write_access(device) == false){ return 0; }
			
			fd = open_fd(device,O_RDWR | O_NONBLOCK);
			if(fd == -1) { return 0; }
			
			if(!isatty(fd)){// check if tty
				printf("not tty\n");
				return 0;
			}
			if(tcgetattr(fd, &config) < 0){// get current termios config
				printf("could not get termios attributes\n");	
				return 0;
			}
			// set the baudrate
			if(cfsetispeed(&config, B9600) < 0 || cfsetospeed(&config, B9600) < 0){
				printf("could not set baudrates");
				return 0;
			}
			if(tcsetattr(fd, TCSAFLUSH, &config) < 0){ // apply new config
				printf("failed to set new termios attributes");
				return 0;
			}

			fd_fifo = open_fd(fifo, O_RDWR);
			if(fd_fifo == -1) {
				return 0;
			}
			while(1){
				if(read(fd_fifo, fifo_buff, 4)){
					if((int)fifo_buff[0] == TERM_BYTE){
						printf("received term byte\n");
						break;
					} else if((int)fifo_buff[0] == OP_SERVO){
						printf("OP_SERVO\t");
						servo_data.axis = (char)fifo_buff[1];
						servo_data.angle = (unsigned short int)fifo_buff[2];

						if(servo_data.axis == 'x' || servo_data.axis == 'y'){
							if(servo_data.angle >= 0 && servo_data.angle <= 180){
								printf("axis: %c\t", servo_data.axis);
								printf("angle: %d\n", servo_data.angle);
								write_servo(fd,p_servo_data);
							}
						}
					}
				}
				usleep(100000);//100ms
			}
			close(fd_fifo);
			printf("fd_fifo connection closed.\n");
			
			close(fd);
			printf("fd connection closed.\n");
			
			unlink(fifo);
			printf("fifo unlinked.\n");
		} else {
			fd_fifo = open_fd(fifo, O_WRONLY);
			if(fd_fifo == -1) {
				return 0;
			}
			if (kill_daemon == true){ // option -q
				printf("kill daemon\n");
				write(fd_fifo,&term,4);
			} else {
				write_servo(fd_fifo,p_servo_data);
				printf("wrote axis:`%c` angle:`%u` to %s\n", p_servo_data->axis, p_servo_data->angle, fifo);
			}
			close(fd_fifo);
			printf("fd_fifo connection closed.\n");
		}
}
