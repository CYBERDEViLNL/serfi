# Serfi
WIP! Controll / ui for servo controlled directional wifi antenna over GPIO


## Requirements
- pigpio or arduino libs
- python
- iw

# Raspberry Pi
## Build servo_rpi.c
 - Edit `GPIO_SERVO_X` and `GPIO_SERVO_Y` in `servo_rpi.c` to your needs.
 - Edit `SERVO_X_MIN`. `SERVO_X_MAX`, `SERVO_Y_MIN` and `SERVO_Y_MAX` in `servo_rpi.c` to your needs.
 - ```gcc -o servo servo_rpi.c -lpigpio -lrt -lpthread```

# Arduino
`servo_arduino.ino` contains the code that will run on your Arduino.
`servo_arduino.c` contains the code to talk to the Arduino.
### Build servo_arduino.c
 - ```gcc -o servo servo_arduino.c```

### Edit and build servo_arduino.ino then upload to Arduino.
 - Edit `SERVO_X_PIN` and `SERVO_Y_PIN` in `servo_arduino.ino` to your needs.
 - Edit `SERVO_X_MIN`. `SERVO_X_MAX`, `SERVO_Y_MIN` and `SERVO_Y_MAX` in `servo_arduino.ino` to your needs.
 - Use `Arduino IDE` or `avr-gcc`, `avrdude` etc to compile and upload the code to your Arduino.

## UI
### General
Key | action
--- | --- 
tab | go to next object
shift-tab | go to previous object

### Table selected
Key | action
--- | --- 
up | go up a row
down | go down a row
\+ | go up 5
\- | go down 5
space | toggle selected row
enter | set sevro to angles off selected row

### Button selected
Key | action
--- | --- 
enter | press
