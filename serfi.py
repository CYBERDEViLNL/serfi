#!/usr/bin/python -u
from pytools.output_handler import output_handler
from pytools.output_handler import panel
from pytools.output_handler import input_field
from pytools.output_handler import table
from pytools.output_handler import label
from pytools.periodical import periodical
import time
import datetime
import sys
from collections import OrderedDict
import subprocess as sub
import re
import os
import shlex
import threading

# TODO:
# 	TODO's
# 	fix string overflow in panel/table/column
# 	stop thread button
# 	load scan file
# 	save scan file
# 	check if sudo session active before running command
#	add checks if interface exists +permissions. maybe select if option
# 	custom wireless scanning with netlink

config = {}
try:
	execfile("serfi.conf", config)
except:
		print("No")
		sys.exit(2)
if config['interface']:
	if_name=config['interface']
if config['dev_path']:
	dev_path=config['dev_path']

def remove_false_readings(list):
	list_ranges = []
	for value in list:
		if not list_ranges:# init
			list_ranges.append([1,range((value -5),(value +5)),[value]])
		else:
			done = False
			for index in range(0,len(list_ranges)):
				if value in list_ranges[index][1]:#if value in range -5 +5
					list_ranges[index][0] += 1
					list_ranges[index][2].append(value)
					done = True

			if not done:# :-)
				list_ranges.append([1,range((value -5),(value +5)),[value]])

	highest = [0,0];#count,index
	for index in range(0,len(list_ranges)):
		if list_ranges[index][0] > highest[0]:
			highest = [list_ranges[index][0],index]
	return list_ranges[highest[1]][2]

def getAverage(list):
	average = False
	for value in list:
		if not average:
			average = value
		else:
			average = (average + value) / 2
	return average

# TODO
# - select arduino or rpi
# - arduino: start arduino fifo if there is none
# - arduino: write directly to fifo
def servo(axis,angle):
	with open(os.devnull, 'w') as devnull:
		sub.call(["./servo", "-" + axis + str(angle),dev_path], stderr=devnull, stdout=devnull)

tmp_results_trigger=0
tmp_results=OrderedDict()
# TODO x1 y1 wtf
# why the need of assigning new vars
def scan(x1,y1,scan_number=5):
	global tmp_results
	global tmp_results_trigger

	x = OrderedDict()
	y = OrderedDict()
	x['start'] = int(x1['start'])
	x['end'] = int(x1['end'])
	x['steps'] = int(x1['steps'])

	y['start'] = int(y1['start'])
	y['end'] = int(y1['end'])
	y['steps'] = int(y1['steps'])
	scan_result_file = open("./cache/scan_results","w")
	t = (y['end'] - (y['start'] - y['steps'])) / y['steps']
	if t == 0: t = 1

	for y_counter in range(0,t):
		y_angle =  y['start'] + (y_counter * y['steps'])
		servo("y", str(y_angle))

		time.sleep(0.5)
		n = (x['end'] - (x['start'] - x['steps'])) / x['steps']
		if n == 0: n = 1
		for x_counter in range(0,n):
			x_angle = x['start'] + (x_counter * x['steps'])

			servo("x", str(x_angle))

			compare_signal_list = dict()
			for scan in range(1,scan_number+1): # scan multiple times
				# Encryption key:on
				# IE: IEEE 802.11i/WPA2 Version 1
				#   Group Cipher : TKIP
				#   Pairwise Ciphers (2) : CCMP TKIP
				#   Authentication Suites (1) : PSK
				#   Authentication Suites (1) : 802.1x
				scan_output = sub.check_output('sudo iwlist ' + if_name + ' scanning | egrep "Cell | ESSID:|Quality=|Channel:([0-9]*)|Encryption key:|IE: IEEE 802.11|Group Cipher|Pairwise Cipher|Authentication Suites"', shell=True)
				cells = scan_output.split("          Cell ")
				id_counter = -1

				for cell in cells:
					if id_counter < 1:
						id_counter +=1
						continue
					else:
						id_counter +=1
						tmp = re.match( r'([0-9]*) - Address: (.*?)\n[\s]+Channel:([0-9])*\n[\s]+Quality=(([0-9]*)/([0-9]*))[\s]+Signal level=-([0-9]*) dBm[\s]+\n[\s]+Encryption key:(on|off)\n[\s]+ESSID:"(.*?)"\n(([\s]+IE: IEEE 802.11[a-z]{1}\/(.*) Version ([0-9]{1})\n)([\s]+Group Cipher : ([A-Z]+)\n)([\s]+Pairwise Ciphers \(([0-9]{1})\) : ([A-Z ]+)\n)([\s]+Authentication Suites \(([0-9]{1})\) : (.*)\n))?', cell, re.M|re.I)
						if tmp.group(1):
							"""	1	cell id
								2	bssid
								3	channel
								4	quality
								5	quality1
								6	quality2
								7	signal
								8	encryption key
								9	essid
									IEEE 802.11 type
									verion
									Group Cipher
								17	Pairwise Ciphers count
								18	Pairwise Ciphers
								20	Authentication Suites count
								21	Authentication Suites
							"""
							cell_id = tmp.group(1)
							bssid = tmp.group(2)
							channel = tmp.group(3)
							essid = tmp.group(9)
							quality = tmp.group(5)
							signal = int(tmp.group(7))
							encryption = "OPN"
							auth_suites = "-"
							pairwise_ciphers = "-"
							if tmp.group(8) == 'on':
								encryption = tmp.group(12)# len 4
								auth_suites = tmp.group(21)# len 9
								pairwise_ciphers = tmp.group(18)#len 6

							if bssid not in compare_signal_list:
								compare_signal_list.update( { bssid : [signal] } )
							else:
								compare_signal_list[bssid].append(signal)

							if scan == scan_number:
								signal_list = remove_false_readings(compare_signal_list[bssid])
								average_signal = getAverage(signal_list)
								output = ("{0},{1},{2},{3},{4},{5},{6},{7},\"{8}\",{9},\"{10}\"\n".format(y_angle, x_angle, cell_id, bssid, quality, average_signal, channel, encryption, pairwise_ciphers, auth_suites, essid))
								scan_result_file.write(output)
								scan_result_file.flush()
								
								# live results
								if bssid not in tmp_results:
									tmp_results[bssid] = [y_angle,x_angle,quality,average_signal,channel, encryption, pairwise_ciphers, auth_suites, essid]
								elif average_signal < tmp_results[bssid][3]:
									tmp_results[bssid][0] = y_angle
									tmp_results[bssid][1] = x_angle
									tmp_results[bssid][2] = quality
									tmp_results[bssid][3] = average_signal
								# end live
						else:
							print("Hmm something's wrong! No matches at all..")
				time.sleep(0.150)
			tmp_results_trigger+=1
	scan_result_file.close()

def handle_scan_cache_file(write=True):
	results = OrderedDict()
	import csv
	with open('./cache/scan_results') as file:
		reader = csv.reader(file, delimiter=",")
		reader = sorted(reader, key=lambda row: row[5])
		for y,x,cellid,bssid,quality,signal,channel,encryption,pairwise_ciphers,auth_suites,essid in reader:
			if bssid not in results:
				results[bssid] = [y,x,quality,signal,channel, encryption, pairwise_ciphers, auth_suites, essid]
			elif signal < results[bssid][3]:
				results[bssid][0] = y
				results[bssid][1] = x
				results[bssid][2] = quality
				results[bssid][3] = signal
	if write:
		write_scan_results(results)
	return results

def write_scan_results(results):
	filename = "./data/scan_{}".format( time.strftime("%Y%m%d%H%M%S") )
	file = open( filename, "w" )
	for mac, values in results.iteritems():
		file.write("{},{},{},{},{},{},{},{},{},'{}'\n".format(mac,values[0],values[1],values[2],values[3],values[4],values[5],values[6],values[7],values[8]))
	file.flush()
	file.close()

def read_scan_results(filename):
	import csv
	results = OrderedDict()

	with open("./data/{}".format(filename)) as file:
		reader = csv.reader(file, delimiter=",")
		reader = sorted(reader, key=lambda row: row[4])
		for bssid,y,x,quality,signal,channel,encryption,pairwise_ciphers,auth_suites,essid in reader:
				results[bssid] = [y,x,quality,signal,channel,encryption,pairwise_ciphers,auth_suites,essid]
	return results

direction = 'forward'
def next(objects):
	global direction
	if direction != 'forward':
		objects = reverse_dict(objects)
		direction = 'forward'
	else:
		name, obj = objects.iteritems().next()
		objects.pop( name )
		objects.update( { name : obj } )
	return objects

def reverse_dict(dict):
	return OrderedDict(dict.items()[::-1])

def previous(objects):
	global direction
	if direction != 'backward':
		objects = reverse_dict(objects)
		direction = 'backward'
	else:
		name, obj = objects.iteritems().next()
		objects.pop( name )
		objects.update( { name : obj } )
	return objects

def update_table1(table1,log_panel,panel1):
	# TODO remove temps and find better solution..
	global tmp_results_trigger
	tmp_1 = tmp_results_trigger
	while scan_threat.is_alive():
		if tmp_results_trigger != tmp_1:
			tmp_1 = tmp_results_trigger
			
			log_panel.update_buffer("Update {}".format(tmp_1))
			log_panel.print_buffer()
			
			# clear the scroll buffer
			# TODO make function for this
			panel1.scroll_buffer = [""]
			panel1.scroll_buffer_count = 0

			# sorting on 3 aka signal
			for bssid, values in sorted(tmp_results.items(), key=lambda r: r[1][3]):
				col = columns.copy()
				col['cb'].append('[ ]')
				col['y'].append(values[0])
				col['x'].append(values[1])
				col['signal'].append("-"+str(values[3]))
				col['c'].append(values[4])
				col['enc'].append(values[5])
				col['auth'].append(values[6])
				col['cipher'].append(values[7])
				col['bssid'].append(bssid)
				col['essid'].append(values[8])

				table1.update_table(col)
				for name,r in col.iteritems():
					r.pop()
			panel1.print_buffer()
		# maybe add a sleep?
		time.sleep(0.2)
	handle_scan_cache_file(True)
	log_panel.update_buffer("[{}] Done scanning :-)".format(str(datetime.datetime.now())[:19]))
							

objects = OrderedDict()
x = OrderedDict()
y = OrderedDict()

scan_file = 'scan_yyyymmddhhMMss'
scan_file = None

with output_handler(config['output']) as out:
	out.header = "Servo Scan"
	out.print_header()
	out.print_buffer()

	panel1 = panel(out,99,30,0,0,255)

	log_panel = panel(out,99,5,0,30,255)
	log_panel.reverse_output = True
	objects['panel1'] = panel1
	objects['panel1'].header = "Yes my friends, yes!"
	
	columns = OrderedDict()
	columns['cb']			= [3] # checkbox
	columns['y']			= [3]
	columns['x']			= [3]
	columns['signal']		= [6]
	columns['c']			= [2]
	columns['enc']			= [4]
	columns['auth']			= [9]
	columns['cipher']		= [6]
	columns['bssid']		= [17]
	columns['essid']		= [26]

	table1 = table(objects['panel1'],columns)

	objects['panel1'].print_buffer()
	log_panel.print_buffer()

	x_label = label(out,18,101,-1,"x min  max  steps ")
	objects.update( { 'input_x_min_angle' : input_field(out,4,103,0) } )
	objects.update( { 'input_x_max_angle' : input_field(out,4,108,0) } )
	objects.update( { 'input_x_steps' : input_field(out,4,113,0) } )
	
	y_label = label(out,18,101,2,"y min  max  steps ")
	objects.update( { 'input_y_min_angle' : input_field(out,4,103,3) } )
	objects.update( { 'input_y_max_angle' : input_field(out,4,108,3) } )
	objects.update( { 'input_y_steps' : input_field(out,4,113,3) } )
	
	scan_num_label = label(out,18,101,5,"scan num")
	objects.update( { 'input_scan_num' : input_field(out,4,101,6) } )
	
	# Defaults
	objects['input_scan_num'].add_text('1')

	objects['input_x_min_angle'].add_text('0')
	objects['input_x_max_angle'].add_text('180')
	objects['input_x_steps'].add_text('10')

	objects['input_y_min_angle'].add_text('50')
	objects['input_y_max_angle'].add_text('90')
	objects['input_y_steps'].add_text('10')
	# End Defaults

	objects.update( { 'scan_button' : label(out,18,101,8,"       SCAN") } )
	objects.update( { 'action_button' : label(out,18,101,10,"      ACTION") } )

	if objects['panel1'].header != None:
		objects['panel1'].print_header()
	
	objects['panel1'].print_buffer()

	prev_get_time = datetime.datetime.now()
	prev_selected_line = [0,'']

	while True:
		change = False
		time.sleep(0.1)
		keys = out.read_input_keys()
		if keys != [] and keys != None:
			out.match_keys(keys)
			if out.key_pressed('esc'):
				break
			elif out.key_pressed('tab'):#tab
				objects.iteritems().next()[1].set_selected(False)
				objects = next(objects)
				objects.iteritems().next()[1].set_selected(True)
			elif out.key_pressed('shift_tab'):
				objects.iteritems().next()[1].set_selected(False)
				objects = previous(objects)
				objects.iteritems().next()[1].set_selected(True)
			if objects.iteritems().next()[1].selected == True:
				if objects.iteritems().next()[1].__class__.__name__ == 'input_field':
					for key in out.matched_keys:
						if len(key) == 1:
							# Numeric
							if objects.iteritems().next()[0] in ['input_scan_num','input_x_min_angle', 'input_x_max_angle', 'input_y_min_angle', 'input_y_max_angle', 'input_x_steps', 'input_y_steps']:
								if ord(key) in range(48,58): # numeric 0-9
									objects.iteritems().next()[1].add_text(key)
							else: # all
								if ord(key) in range(32,126):
									objects.iteritems().next()[1].add_text(key)
						elif out.key_pressed('backspace'):
							objects.iteritems().next()[1].backspace()
						elif out.key_pressed('left'):
							objects.iteritems().next()[1].pos -= 1
						elif out.key_pressed('right'):
							objects.iteritems().next()[1].pos += 1
						elif out.key_pressed('return'):
							if objects.iteritems().next()[0] in ['input_x_min_angle', 'input_x_max_angle']:
								log_panel.update_buffer("[{}] Setting angle x {}".format(str(datetime.datetime.now())[:19], objects.iteritems().next()[1].input_txt))
								log_panel.print_buffer()
								servo("x",objects.iteritems().next()[1].input_txt)
							elif objects.iteritems().next()[0] in ['input_y_min_angle', 'input_y_max_angle']:
								log_panel.update_buffer("[{}] Setting angle y {}".format(str(datetime.datetime.now())[:19], objects.iteritems().next()[1].input_txt))
								log_panel.print_buffer()
								servo("y",objects.iteritems().next()[1].input_txt)
				elif objects.iteritems().next()[1].__class__.__name__ == 'panel':
					if objects.iteritems().next()[1].selected_line == -1:
						objects.iteritems().next()[1].selected_line = 0
					if out.key_pressed('up'):
						if objects.iteritems().next()[1].selected_line != 0:
							objects.iteritems().next()[1].selected_line += -1
						elif objects.iteritems().next()[1].scroll_pos > 0 and objects.iteritems().next()[1].scroll_buffer_count > objects.iteritems().next()[1].height:
							objects.iteritems().next()[1].scroll_pos += -1
					elif out.key_pressed('down'):
						if objects.iteritems().next()[1].selected_line == objects.iteritems().next()[1].height-2:
							if objects.iteritems().next()[1].scroll_pos < (objects.iteritems().next()[1].scroll_buffer_count-1 -objects.iteritems().next()[1].height):
								objects.iteritems().next()[1].scroll_pos += 1
						elif objects.iteritems().next()[1].selected_line < len(objects.iteritems().next()[1].scroll_buffer)-1:
							objects.iteritems().next()[1].selected_line += 1
					elif out.key_pressed(' '):
						line_num = objects.iteritems().next()[1].selected_line + objects.iteritems().next()[1].scroll_pos
						if table1.get_column_value(line_num,'cb') == '[ ]':
							table1.update_column(line_num, 'cb', '[x]')
						else:
							table1.update_column(line_num, 'cb', '[ ]')
					elif out.key_pressed('-'):
						if objects.iteritems().next()[1].selected_line+5 >= objects.iteritems().next()[1].height-2:
							if objects.iteritems().next()[1].scroll_pos+5 < (objects.iteritems().next()[1].scroll_buffer_count-1 -objects.iteritems().next()[1].height):
								objects.iteritems().next()[1].scroll_pos += 5
							else:
								objects.iteritems().next()[1].scroll_pos = (objects.iteritems().next()[1].scroll_buffer_count-1 -objects.iteritems().next()[1].height)
								objects.iteritems().next()[1].selected_line = objects.iteritems().next()[1].height - 2
						elif objects.iteritems().next()[1].selected_line+5 < len(objects.iteritems().next()[1].scroll_buffer)-1:
							objects.iteritems().next()[1].selected_line += 5
					elif out.key_pressed('='):
						if objects.iteritems().next()[1].selected_line-5 != 0:
							objects.iteritems().next()[1].selected_line += -5
						if objects.iteritems().next()[1].scroll_pos-5 > 0 and objects.iteritems().next()[1].scroll_buffer_count > objects.iteritems().next()[1].height:
							objects.iteritems().next()[1].scroll_pos += -5
						else:
							objects.iteritems().next()[1].scroll_pos = 0
							objects.iteritems().next()[1].selected_line = 0
					elif out.key_pressed('return'):
						log_panel.update_buffer("[{}] Setting angles (y,x) {}, {}".format(str(datetime.datetime.now())[:19], table1.get_column_value(objects.iteritems().next()[1].selected_line + objects.iteritems().next()[1].scroll_pos,'y').rstrip(),table1.get_column_value(objects.iteritems().next()[1].selected_line + objects.iteritems().next()[1].scroll_pos,'x').rstrip()))
						log_panel.print_buffer()
						servo("x",table1.get_column_value(objects.iteritems().next()[1].selected_line + objects.iteritems().next()[1].scroll_pos,'x').rstrip())
						servo("y",table1.get_column_value(objects.iteritems().next()[1].selected_line + objects.iteritems().next()[1].scroll_pos,'y').rstrip())
				elif objects.iteritems().next()[1].__class__.__name__ == 'label':
					if objects.iteritems().next()[0] == 'scan_button':
						if out.key_pressed('return'):
							if scan_file != None:
								results = read_scan_results(scan_file)
							else:
								log_panel.update_buffer("[{}] Start scanning.. please wait..".format(str(datetime.datetime.now())[:19]))
								
								# TODO make function for this
								panel1.scroll_buffer = [""]
								panel1.scroll_buffer_count = 0
								
								# scan thread
								scan_threat = threading.Thread(target=scan, args=(x,y,int(objects['input_scan_num'].input_txt)))
								scan_threat.daemon = True
								scan_threat.start()
								
								# update table thread
								update_table_thread = threading.Thread(target=update_table1, args=(table1,log_panel,objects['panel1']))
								update_table_thread.daemon = True
								update_table_thread.start()

					elif objects.iteritems().next()[0] == 'action_button':
						if out.key_pressed('return'):
							for row_num in range(0,objects['panel1'].scroll_buffer_count - 1):
								vals = table1.get_column_values(row_num)
								if vals['cb'] == '[x]':
									log_panel.update_buffer("[{}] Setting angles (y,x) {}, {}  {}  {}".format(str(datetime.datetime.now())[:19], vals['y'],vals['x'],vals['signal'],vals['essid'].rstrip()))
									log_panel.print_buffer()
									
									# TODO why y = x and x = y ??
									servo("y",vals['y'])
									servo("x",vals['x'])
									time.sleep(0.2)

									# Hook call for each checked line

							log_panel.update_buffer("Done")
			objects['panel1'].changed = True

		# Need to do check if anything changed then print
		if objects['input_x_min_angle'].changed:
			objects['input_x_min_angle'].print_field()
			x['start'] = objects['input_x_min_angle'].input_txt

		if objects['input_x_max_angle'].changed:
			objects['input_x_max_angle'].print_field()
			x['end'] = objects['input_x_max_angle'].input_txt

		if objects['input_x_steps'].changed:
			objects['input_x_steps'].print_field()
			x['steps'] = objects['input_x_steps'].input_txt

		if objects['input_y_min_angle'].changed:
			objects['input_y_min_angle'].print_field()
			y['start'] = objects['input_y_min_angle'].input_txt

		if objects['input_y_max_angle'].changed:
			objects['input_y_max_angle'].print_field()
			y['end'] = objects['input_y_max_angle'].input_txt

		if objects['input_y_steps'].changed:
			objects['input_y_steps'].print_field()
			y['steps'] = objects['input_y_steps'].input_txt

		if objects['input_scan_num'].changed:
			objects['input_scan_num'].print_field()

		if objects['scan_button'].changed:
			objects['scan_button'].print_label()

		if objects['action_button'].changed:
			objects['action_button'].print_label()

		if log_panel.changed:
			log_panel.print_buffer()

		#if change or objects['panel1'].changed:
		# just print it..
		objects['panel1'].print_buffer()
		
		time.sleep(0.1) # still running in while loop ;-)
