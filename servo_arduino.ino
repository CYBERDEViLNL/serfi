#include <Servo.h>
Servo servo_x;
Servo servo_y;

#define SERVO_X_PIN  9
#define SERVO_Y_PIN  10

#define SERVO_X_MIN  0
#define SERVO_X_MAX  180

#define SERVO_Y_MIN  0
#define SERVO_Y_MAX  110

#define TERM_BYTE	 0x00
#define EXEC_BYTE	 0x01
#define OP_SERVO	 0x02
#define DATA_OK		 0x03

int x_pos = 0;
int y_pos = 0;
struct servo_data {
  char axis = -1;
  int angle = -1;
};
struct instruction {
  char opcode = -1;
};

char recv_byte;
char buff[10];
bool trigger = false;

// init structs
struct instruction new_instruction;
struct servo_data new_servo_data;
static const struct servo_data clear_servo_data;
static const struct instruction clear_instruction;

void setup() {
  // setup serial communication
  Serial.begin(9600);
}

void loop() {
  if (Serial.available() > 0) {
    recv_byte = Serial.read();
    trigger = true;
    //Serial.print("Received: ");
    //Serial.println(recv_byte);
  }
  if(trigger){
    trigger = false;

    // read data
    if(new_instruction.opcode){
      switch(new_instruction.opcode){
        case OP_SERVO:
          if(new_servo_data.axis == -1){
            // collect axis
            if(recv_byte == 'x' || recv_byte == 'y' ){
              new_servo_data.axis = recv_byte;
              //Serial.print("SET AXIS: ");
              //Serial.println(new_servo_data.axis);
            } else{
              // reset instructions, something went bad..
              // doing so by copying the fresh clear_instruction struct into new_instruction.
              new_instruction = clear_instruction;
              new_servo_data = clear_servo_data; // just to be sure.
            }
          } else if(new_servo_data.angle == -1){
            // collect angle
            int tmp = (unsigned char) recv_byte;
            if(tmp >= 0 && tmp <= 180){
              //Serial.print("SET ANGLE: ");
              //Serial.println(tmp);
              new_servo_data.angle = (unsigned char) recv_byte;
            }
          } else if((int)recv_byte == EXEC_BYTE) {
            // execute
            Serial.write((char)EXEC_BYTE);
            switch(new_servo_data.axis){
              case 'x':
                if(new_servo_data.angle >= SERVO_X_MIN && new_servo_data.angle <= SERVO_X_MAX){
                  servo_x.attach(SERVO_X_PIN);
                  servo_x.write(new_servo_data.angle);
                  delay(500);
                  servo_x.detach();
                }
                break;
              case 'y':
                if(new_servo_data.angle >= SERVO_Y_MIN && new_servo_data.angle <= SERVO_Y_MAX){
                  servo_y.attach(SERVO_Y_PIN);
                  servo_y.write(new_servo_data.angle);
                  delay(500);
                  servo_y.detach();
                }
                break;
            }
            Serial.write((char)DATA_OK);
            
            // and reset struct
            new_servo_data = clear_servo_data;
            new_instruction = clear_instruction;
          }
          break;
      }
    }
    // read opcode
    if((int)recv_byte == OP_SERVO){
      Serial.write((char)OP_SERVO);
      new_instruction.opcode = OP_SERVO;
    }
  }
}
