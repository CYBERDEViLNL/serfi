#include <stdio.h>
#include <pigpio.h>
#include <unistd.h>

#define GPIO_SERVO_X 17
#define GPIO_SERVO_Y 27

#define SERVO_X_MIN  0
#define SERVO_X_MAX  180

#define SERVO_Y_MIN  0
#define SERVO_Y_MAX  100

struct servoData {
  char axis;
  unsigned char angle;
};

int main(int argc, char *argv[]){
	struct servoData servo_data;
	// dump for stroul
	char *strtoul_tmp = NULL;
	// parse arguments
	int c;
	while ((c = getopt (argc, argv, "x:y:")) != -1) {
		switch(c){
			case 'x': case 'y':
				if(optarg){
					servo_data.axis = (char)c;
					if(strtoul(optarg, &strtoul_tmp, 0) >= 0 && strtoul(optarg, &strtoul_tmp, 0) <= 180){
						servo_data.angle = strtoul(optarg, &strtoul_tmp, 0);
					}
				}
				break;
		}
	}
	if (gpioInitialise() < 0){
		fprintf(stderr, "pigpio initialisation failed\n");
		return 1;
	}

	float multiplier = 2000.0f / 180.0f;

	int ms = (servo_data.angle * multiplier) + 500;

	gpioSetMode(GPIO_SERVO_X, PI_OUTPUT);
	gpioSetMode(GPIO_SERVO_Y, PI_OUTPUT);

	switch (servo_data.axis){
		case 'x' :
			if(servo_data.angle >= SERVO_X_MIN && servo_data.angle <= SERVO_X_MAX){
				gpioServo(GPIO_SERVO_X, ms);
			}
			else{
				printf("x axis out of range, angle: %d\n", servo_data.angle);
				return 1;
			}
			break;
		case 'y' :
			if(servo_data.angle >= SERVO_Y_MIN && servo_data.angle <= SERVO_Y_MAX){
				gpioServo(GPIO_SERVO_Y, ms);
			}
			else{
				printf("y axis out of range, angle: %d\n", servo_data.angle);
				return 1;
			}
			break;
	}
	printf("axis: %c angle: %d\n", servo_data.axis, servo_data.angle);
	time_sleep(0.8);

	gpioTerminate();
	return 0;
}
